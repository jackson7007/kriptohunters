extends Node2D
var myChar: Dictionary

export var scenarios: Array
export (int) var speed = 200

var moveAct:bool = false
var vel: = Vector2()
var pos_final: bool = false 

# Called when the node enters the scene tree for the first time.
func _ready():
	print("Battle")
	print(RequestPlayer.my_char_batle)
	$Start.play()


func _on_AnimationPlayer_animation_finished(anim_name):
	$Player/full_body/AnimationPlayer.play("run")
	$Enemy/full_body/AnimationPlayer.play("run")
	yield(get_tree().create_timer(2.5), "timeout")
	$war_horn.stop()
	$Enemy/full_body.visible = false
	$Player/full_body.visible = false
	$battle.play()
	


func _on_Start_finished():
	$Start.stop()
	$Start.visible = false
	$Start.queue_free()
	moveAct = true
	$AnimationPlayer.play("scroll")
	$war_horn.play()


func _on_battle_finished():
	$battle.stop()
	$battle.queue_free()
	$victory.play()
