extends Node2D

var rng
signal player_raritie(rarity)

export var possible_char : = {
	"warrior": {
		"common": [],
		"uncommon": [],
		"rare": [],
		"epic": [],
		"legendary": [],
	},
	"mage": {
		"common": [],
		"uncommon": [],
		"rare": [],
		"epic": [],
		"legendary": [],
	},
	"assassin": {
		"common": [],
		"uncommon": [],
		"rare": [],
		"epic": [],
		"legendary": [],
	},
}


func _ready():
	GameState.connect("result", self, "_on_Request_result")
	randomize()
	rng = RandomNumberGenerator.new()
	rng.randomize()
	$full_body/AnimationPlayer.play("Idle")


func create_armor(class_char: String) ->void:
	resetImg()
	$full_body.generate()
	
	generate_Char(class_char)
	


func generate_Char(class_char: String) -> void:
	print("sinal")
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	#var choicedChar = result.storage[rng.randi_range(0, result.storage.size() -1)]
	var arrayChar: Array = possible_char[class_char][$full_body.rarity.rarity_name]
	var choicedChar = arrayChar[rng.randi_range(0, arrayChar.size() -1)]
	texture_load(choicedChar.body_char)
	generate_status(choicedChar)


func LoadCharacter(character: Dictionary) -> void :
	print("sinal inventory")
	print(character)
	generate_body_char(character)


func generate_body_char (choicedChar: Dictionary) ->void :
	var selected_body : Object
	var selectedClassType: Array = possible_char[choicedChar.class][choicedChar.type]

	for i in selectedClassType.size() :
		if choicedChar.char_id ==  selectedClassType[i].char_id :
			selected_body = selectedClassType[i]
	
	texture_load(selected_body.body_char)
	choicedChar.soucer_game_type = selected_body.soucer_game_type
	emit_signal("player_raritie", choicedChar)


func texture_load(_selectedTexture):
	
	print("CARREGADO E ATUALIZADO")
	print(str(_selectedTexture))
	$full_body/Cape/Sprite.texture = load(_selectedTexture.cape)
	$full_body/Head.texture = load(_selectedTexture.head)
	$full_body/Head/Helmet.texture = load(_selectedTexture.helmet)
	$full_body/Head/Hair.texture = load(_selectedTexture.hair)
	$full_body/Head/Ear.texture = load(_selectedTexture.ear)
	$full_body/Head/Ear/Earrings/Sprite.texture = load(_selectedTexture.earrings)
	$full_body/Head/Mouth.texture = load(_selectedTexture.mouth)
	$full_body/Head/Beard.texture = load(_selectedTexture.beard)
	$full_body/Head/Eyes.texture = load(_selectedTexture.eyes)
	$full_body/Head/Eyebrows.texture = load(_selectedTexture.eyebrows)
	$full_body/Left_arm/Sprite_formearm/Shield/Sprite.texture = load(_selectedTexture.shield)
	$full_body/Right_arm/Sprite_forearm/Sprite_hand/right_hand_armor/Weapon/Sprite.texture = load(_selectedTexture.weapon)
	#body
	#left arm
	$full_body/Left_arm.texture = load(_selectedTexture.body)
	$full_body/Left_arm/Sprite_formearm.texture = load(_selectedTexture.body)
	$full_body/Left_arm/Sprite_formearm/Sprite_finger.texture = load(_selectedTexture.body)
	$full_body/Left_arm/Sprite_formearm/Sprite_hand.texture = load(_selectedTexture.body)
	#rigth arm
	$full_body/Right_arm.texture = load(_selectedTexture.body)
	$full_body/Right_arm/Sprite_forearm.texture = load(_selectedTexture.body)
	$full_body/Right_arm/Sprite_forearm/Sprite_hand.texture = load(_selectedTexture.body)
	#Torax
	$full_body/thorax.texture = load(_selectedTexture.body)
	#hip
	$full_body/Hip.texture = load(_selectedTexture.body)
	#left Leg
	$full_body/Hip/Left_leg.texture = load(_selectedTexture.body)
	$full_body/Hip/Left_leg/Sprite_foot.texture = load(_selectedTexture.body)
	#right leg
	$full_body/Hip/Right_leg.texture = load(_selectedTexture.body)
	$full_body/Hip/Right_leg/Sprite_foot.texture = load(_selectedTexture.body)
	#armor
	#left arm Armor
	$full_body/Left_arm/Left_arm_armor.texture = load(_selectedTexture.armor)
	$full_body/Left_arm/Sprite_formearm/right_formearm_armor.texture = load(_selectedTexture.armor)
	$full_body/Left_arm/Sprite_formearm/Sprite_finger/right_finger_armor.texture = load(_selectedTexture.armor)
	$full_body/Left_arm/Sprite_formearm/Sprite_hand/right_hand_armor.texture = load(_selectedTexture.armor)
	#rigth arm armor
	$full_body/Right_arm/Right_arm_armor.texture = load(_selectedTexture.armor)
	$full_body/Right_arm/Sprite_forearm/right_forearm_armor.texture = load(_selectedTexture.armor)
	$full_body/Right_arm/Sprite_forearm/Sprite_hand/right_hand_armor.texture = load(_selectedTexture.armor)
	#Torax
	$full_body/thorax/thorax_armor.texture = load(_selectedTexture.armor)
	#hip
	$full_body/Hip/Hip_armor.texture = load(_selectedTexture.armor)
	#left Leg
	$full_body/Hip/Left_leg/Left_leg_armor.texture = load(_selectedTexture.armor)
	$full_body/Hip/Left_leg/Sprite_foot/Sprite_foot_armor.texture = load(_selectedTexture.armor)
	#right leg
	$full_body/Hip/Right_leg/Right_leg_armor.texture = load(_selectedTexture.armor)
	$full_body/Hip/Right_leg/Sprite_foot/Sprite_foot_armor.texture = load(_selectedTexture.armor)


func resetImg() -> void :
	$full_body/Cape/Sprite.texture = null
	$full_body/Head.texture = null
	$full_body/Head/Helmet.texture = null
	$full_body/Head/Hair.texture = null
	$full_body/Head/Ear.texture = null
	$full_body/Head/Ear/Earrings/Sprite.texture = null
	$full_body/Head/Mouth.texture = null
	$full_body/Head/Beard.texture = null
	$full_body/Head/Eyes.texture = null
	$full_body/Head/Eyebrows.texture = null
	$full_body/Left_arm/Sprite_formearm/Shield/Sprite.texture = null
	$full_body/Right_arm/Sprite_forearm/Sprite_hand/right_hand_armor/Weapon/Sprite.texture = null
	#left arm Armor
	$full_body/Left_arm.texture = null
	$full_body/Left_arm/Sprite_formearm.texture = null
	$full_body/Left_arm/Sprite_formearm/Sprite_finger.texture = null
	$full_body/Left_arm/Sprite_formearm/Sprite_hand.texture = null
	#rigth arm armor
	$full_body/Right_arm.texture = null
	$full_body/Right_arm/Sprite_forearm.texture = null
	$full_body/Right_arm/Sprite_forearm/Sprite_hand.texture = null
	#Torax
	$full_body/thorax.texture = null
	#hip
	$full_body/Hip.texture = null
	#left Leg
	$full_body/Hip/Left_leg.texture = null
	$full_body/Hip/Left_leg/Sprite_foot.texture = null
	#right leg
	$full_body/Hip/Right_leg.texture = null
	$full_body/Hip/Right_leg/Sprite_foot.texture = null
	#left arm Armor
	$full_body/Left_arm/Left_arm_armor.texture = null
	$full_body/Left_arm/Sprite_formearm/right_formearm_armor.texture = null
	$full_body/Left_arm/Sprite_formearm/Sprite_finger/right_finger_armor.texture = null
	$full_body/Left_arm/Sprite_formearm/Sprite_hand/right_hand_armor.texture = null
	#rigth arm armor
	$full_body/Right_arm/Right_arm_armor.texture = null
	$full_body/Right_arm/Sprite_forearm/right_forearm_armor.texture = null
	$full_body/Right_arm/Sprite_forearm/Sprite_hand/right_hand_armor.texture = null
	#Torax
	$full_body/thorax/thorax_armor.texture = null
	#hip
	$full_body/Hip/Hip_armor.texture = null
	#left Leg
	$full_body/Hip/Left_leg/Left_leg_armor.texture = null
	$full_body/Hip/Left_leg/Sprite_foot/Sprite_foot_armor.texture = null
	#right leg
	$full_body/Hip/Right_leg/Right_leg_armor.texture = null
	$full_body/Hip/Right_leg/Sprite_foot/Sprite_foot_armor.texture = null


func generate_status(rarity_source):
	var random_status : = 0
	var new_Diff : = 0
	var range_init : = 0
	
	
	if rarity_source.type == "common" :
		random_status = rng.randi_range(range_init, 300)
	elif rarity_source.type == "uncommon" :
		range_init = 300
		random_status = rng.randi_range(range_init, 600)
	elif rarity_source.type == "rare" :
		range_init = 600
		random_status = rng.randi_range(range_init, 1200)
	elif rarity_source.type == "epic" :
		range_init = 1200
		random_status = rng.randi_range(range_init, 2400)
	elif rarity_source.type == "legendary" :
		range_init = 2400
		random_status = rng.randi_range(range_init, 4800)
	
	new_Diff = randi() % random_status
	random_status -= new_Diff
	var atk = new_Diff
	new_Diff = randi() % random_status
	random_status -= new_Diff
	var deff = new_Diff
	new_Diff = random_status
	var health = new_Diff
	
	var obj = {
		"type": rarity_source.type,
		"class": rarity_source.name_class,
		"char_name": rarity_source.char_name,
		"char_id": rarity_source.char_id,
		"level": 1,
		"experience":0,
		"url": rarity_source.url,
		"attack": atk,
		"defense": deff,
		"health": health,
		#"user_id": choicedChar.user_id
		"soucer_game_type": rarity_source.soucer_game_type
	}
	GameState.player_data.storage.append(obj)
	emit_signal("player_raritie", obj)

