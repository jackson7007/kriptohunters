extends Node

var player_data

func _ready():
	var data = get_parameter("id")
	if !data :
		data = "1"
		var body = {
			user_id = int(data)
		}
	
func get_parameter(parameter):
	if OS.has_feature('JavaScript'):
		return JavaScript.eval(""" 
				var url_string = window.location.href;
				var url = new URL(url_string);
				url.searchParams.get(parameter);
			""")
	return null
