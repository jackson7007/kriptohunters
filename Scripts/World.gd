extends Node
var rng
var myChar: Dictionary
var MouseOver = false

func _ready():
	randomize()
	rng = RandomNumberGenerator.new()
	rng.randomize()
	$Control/Player.connect("player_raritie", self, "_on_Player_player_raritie")


"""func _process(delta):
	if Input.is_action_just_pressed("ui_up"):
		$Control/Player.create_armor("warrior")
	if Input.is_action_just_pressed("ui_down"):
		$Control/Player/full_body/AnimationPlayer.play("attack")
		yield($Control/Player/full_body/AnimationPlayer, "animation_finished")
		$Control/Player/full_body/AnimationPlayer.play("Idle")"""


func LoadPlayerStorage(character: Dictionary) -> void :
	myChar = character
	$Control/Player.LoadCharacter(character)

func CreatePlayerStorage(character: String) -> void :
	$Control/Player.create_armor(character)


func _on_Button2_pressed():
	$Control/Player/full_body/AnimationPlayer.play("attack")
	yield($Control/Player/full_body/AnimationPlayer, "animation_finished")
	$Control/Player/full_body/AnimationPlayer.play("Idle")


func _on_Player_player_raritie(rarity_source):
	$Control/rarity_card.play(rarity_source.type)
	$Control/Label.text = rarity_source.char_name
	$Control/Label.add_color_override("font_color", rarity_source.soucer_game_type.ui_color)
	$Control/Lable_attack.text = str(rarity_source.attack)
	$Control/Lable_Defense.text = str(rarity_source.defense)
	$Control/Lable_health.text = str(rarity_source.health)


func _input(event):
	if event is InputEventMouseButton and (event.pressed == true) and event.button_index == BUTTON_LEFT:
		if MouseOver == true:
			RequestPlayer.my_char_batle = myChar
			get_tree().change_scene("res://Scenes/Battle.tscn")



func _on_World_mouse_entered():
	MouseOver = true


func _on_World_mouse_exited():
	MouseOver = false
