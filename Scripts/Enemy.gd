extends Node2D

var rng
signal player_raritie(rarity)

export var numberEnemy: = [] 


func _ready():
	$full_body/AnimationPlayer.play("Idle")
	texture_load()


func texture_load():
	
	print("CARREGADO E ATUALIZADO")
	var _selectedTexture = numberEnemy[0].body_char
	print(str(_selectedTexture))
	$full_body/Cape/Sprite.texture = load(_selectedTexture.cape)
	$full_body/Head.texture = load(_selectedTexture.head)
	$full_body/Head/Helmet.texture = load(_selectedTexture.helmet)
	$full_body/Head/Hair.texture = load(_selectedTexture.hair)
	$full_body/Head/Ear.texture = load(_selectedTexture.ear)
	$full_body/Head/Ear/Earrings/Sprite.texture = load(_selectedTexture.earrings)
	$full_body/Head/Mouth.texture = load(_selectedTexture.mouth)
	$full_body/Head/Beard.texture = load(_selectedTexture.beard)
	$full_body/Head/Eyes.texture = load(_selectedTexture.eyes)
	$full_body/Head/Eyebrows.texture = load(_selectedTexture.eyebrows)
	$full_body/Left_arm/Sprite_formearm/Shield/Sprite.texture = load(_selectedTexture.shield)
	$full_body/Right_arm/Sprite_forearm/Sprite_hand/right_hand_armor/Weapon/Sprite.texture = load(_selectedTexture.weapon)
	#body
	#left arm
	$full_body/Left_arm.texture = load(_selectedTexture.body)
	$full_body/Left_arm/Sprite_formearm.texture = load(_selectedTexture.body)
	$full_body/Left_arm/Sprite_formearm/Sprite_finger.texture = load(_selectedTexture.body)
	$full_body/Left_arm/Sprite_formearm/Sprite_hand.texture = load(_selectedTexture.body)
	#rigth arm
	$full_body/Right_arm.texture = load(_selectedTexture.body)
	$full_body/Right_arm/Sprite_forearm.texture = load(_selectedTexture.body)
	$full_body/Right_arm/Sprite_forearm/Sprite_hand.texture = load(_selectedTexture.body)
	#Torax
	$full_body/thorax.texture = load(_selectedTexture.body)
	#hip
	$full_body/Hip.texture = load(_selectedTexture.body)
	#left Leg
	$full_body/Hip/Left_leg.texture = load(_selectedTexture.body)
	$full_body/Hip/Left_leg/Sprite_foot.texture = load(_selectedTexture.body)
	#right leg
	$full_body/Hip/Right_leg.texture = load(_selectedTexture.body)
	$full_body/Hip/Right_leg/Sprite_foot.texture = load(_selectedTexture.body)
	#armor
	#left arm Armor
	$full_body/Left_arm/Left_arm_armor.texture = load(_selectedTexture.armor)
	$full_body/Left_arm/Sprite_formearm/right_formearm_armor.texture = load(_selectedTexture.armor)
	$full_body/Left_arm/Sprite_formearm/Sprite_finger/right_finger_armor.texture = load(_selectedTexture.armor)
	$full_body/Left_arm/Sprite_formearm/Sprite_hand/right_hand_armor.texture = load(_selectedTexture.armor)
	#rigth arm armor
	$full_body/Right_arm/Right_arm_armor.texture = load(_selectedTexture.armor)
	$full_body/Right_arm/Sprite_forearm/right_forearm_armor.texture = load(_selectedTexture.armor)
	$full_body/Right_arm/Sprite_forearm/Sprite_hand/right_hand_armor.texture = load(_selectedTexture.armor)
	#Torax
	$full_body/thorax/thorax_armor.texture = load(_selectedTexture.armor)
	#hip
	$full_body/Hip/Hip_armor.texture = load(_selectedTexture.armor)
	#left Leg
	$full_body/Hip/Left_leg/Left_leg_armor.texture = load(_selectedTexture.armor)
	$full_body/Hip/Left_leg/Sprite_foot/Sprite_foot_armor.texture = load(_selectedTexture.armor)
	#right leg
	$full_body/Hip/Right_leg/Right_leg_armor.texture = load(_selectedTexture.armor)
	$full_body/Hip/Right_leg/Sprite_foot/Sprite_foot_armor.texture = load(_selectedTexture.armor)

