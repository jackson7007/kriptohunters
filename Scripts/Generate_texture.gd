extends Node2D

signal texture_load(_textureSelected, _type)

var _typeSelected
func _ready():
	RequestPlayer.connect("result_player", self, "on_ready_request")


func on_ready_request(body) -> void:
	print("carregando imagem no server!")
	var image = Image.new()
	var error = image.load_png_from_buffer(body)
	if error != OK:
		push_error("Couldn't load the image.")
	
	var textureSelected = ImageTexture.new()
	textureSelected.create_from_image(image)
	print("TERMINOU!!!!!!!!!!!!!!!!!!!")
	emit_signal("texture_load", textureSelected, _typeSelected) 


func generate_texture(url, type) -> void:
	_typeSelected = type
	
	print(url+ "/" + type + ".png")
	RequestPlayer.send_request_get(url+ "/" + type + ".png")

