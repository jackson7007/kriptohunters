extends Node

func _ready():
	get_tree().get_root().set_transparent_background(true)
	GameState.connect("result", self, "_on_Request_result")

func _on_Request_result(result) -> void:
	queue_free()
	get_tree().change_scene("res://Scenes/Inventary.tscn")
	#get_tree().change_scene("res://Scenes/World.tscn")
