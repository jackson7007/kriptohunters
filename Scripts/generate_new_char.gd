extends Node2D



func _ready():
	get_tree().get_root().set_transparent_background(true)
	GameState.connect("result", self, "_on_Request_result")
	


func _on_Request_result(result) -> void:
	$World.CreatePlayerStorage("warrior")


func _on_Timer_timeout():
	if $Video :
		$World.visible = true
		$AnimationPlayer.play("zoom_out")
		yield(get_tree().create_timer(5.0), "timeout")
		$Inventory.visible = true
		$Timer.queue_free()


func _on_Timer2_timeout():
	$AudioStreamPlayer.playing = true


func _on_Inventory_pressed():
	get_tree().change_scene("res://Scenes/Inventary.tscn")
