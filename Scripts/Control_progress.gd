extends Control

var dots = ""

func _ready():
	pass # Replace with function body.



func _on_Timer_timeout():
	
	dots += "."
	if dots == "...." :
		dots = ""
	
	$Label.text = "Loading " + dots
	$TextureProgress.value += 1
	$percent.text = str($TextureProgress.value) + "%"

func stop() :
	$Timer.stop()
	dots = ""
	$Label.text = "Loading" + dots
