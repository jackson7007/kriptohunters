extends GridContainer

onready var card = preload("res://Scenes/World.tscn")
var sizeRect: Vector2 =Vector2(450, 650)


# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().get_root().set_transparent_background(true)
	for i in GameState.player_data.storage.size():
		var slot = card.instance()
		slot.LoadPlayerStorage(GameState.player_data.storage[i])
		slot.set_custom_minimum_size(sizeRect)
		slot.connect("player_raritie", slot, "_on_Player_player_raritie")
		add_child(slot)
		
