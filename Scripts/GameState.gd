extends Node

onready var http: HTTPRequest = HTTPRequest.new()
signal result(value)

var player_data: Dictionary

func _ready():
	add_child(http)
	http.connect("request_completed",self,"_on_HTTP_request_completed")
	send_request_post("https://www.hunters.cryptoinveste.com.br/api/start", {})


func send_request_post(url:String, body:Dictionary):
	print("sending post")
	var csrf = JavaScript.eval("document.querySelector('meta[name=\"csrf-token\"]').content")
	
	if csrf == null:
		csrf = ""
	 
	var _result = http.request(url, ["content-type: application/json", "X-CSRF-TOKEN: " + csrf], false, HTTPClient.METHOD_POST, to_json(body))


func send_request_get(url: String):
	print("sending get")
	var _result = http.request(url,[],false,HTTPClient.METHOD_GET,"")


func _on_HTTP_request_completed(result, _response_code, _headers, body ):
	print("got response")
	if body :
		var json = JSON.parse(body.get_string_from_utf8())
		print(json.result)
		if json.result != null :
			player_data = json.result
			print(player_data)
		else:
			player_data = {}
	else:
		player_data = {}
	
	var obj = JSON.parse('{"id":3,"name":"Cade616b1dd7","balance":0,"storage":[{"id":1,"user_id":"616b1dd7","char_name":"Iron Soldier","attack":"15","defense":"20","health":"50","level":"1","experience":"0","char_id":3,"class":"warrior","type":"common","url":"https:\/\/www.hunters.cryptoinveste.com.br\/assets\/images\/items\/sprites\/warrior\/common\/3"},{"id":2,"user_id":"616b1dd7","char_name":"Warrior King","attack":"40","defense":"45","health":"60","level":"2","experience":"0","char_id":3,"class":"warrior","type":"uncommon","url":"https:\/\/www.hunters.cryptoinveste.com.br\/assets\/images\/items\/sprites\/warrior\/uncommon\/3"},{"id":3,"user_id":"616b1dd7","char_name":"Shadow Knight","attack":"90","defense":"85","health":"95","level":"3","experience":"0","char_id":3,"class":"warrior","type":"rare","url":"https:\/\/www.hunters.cryptoinveste.com.br\/assets\/images\/items\/sprites\/warrior\/rare\/3"},{"id":4,"user_id":"616b1dd7","char_name":"Epic Necromancer","attack":"90","defense":"85","health":"95","level":"3","experience":"0","char_id":2,"class":"mage","type":"epic","url":"https:\/\/www.hunters.cryptoinveste.com.br\/assets\/images\/items\/sprites\/mage\/epic\/2"},{"id":5,"user_id":"616b1dd7","char_name":"Legendary Assasin","attack":"90","defense":"85","health":"95","level":"3","experience":"0","char_id":1,"class":"assassin","type":"legendary","url":"https:\/\/www.hunters.cryptoinveste.com.br\/assets\/images\/items\/sprites\/assassin\/legendary\/1"}]}')
	player_data = obj.result
	
	emit_signal("result", player_data)

