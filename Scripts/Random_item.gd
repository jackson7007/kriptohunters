extends Node2D
class_name RandomItem

export var possible_rarities : = []
var rarity : Resource

func _ready():
	print(possible_rarities)


func generate() -> void:
	rarity = generate_rarity()


func generate_rarity() -> Resource:
	var rarity_index : = 0
	var rarer_chance : = 1.0
	var roll : = randf()
	for index in possible_rarities.size():
		if roll > possible_rarities[index].chance:
			continue
		if possible_rarities[index].chance > rarer_chance:
			continue
		rarer_chance = possible_rarities[index].chance
		rarity_index = index
	return possible_rarities[rarity_index]

