extends TextureButton


var pressdown_position = Vector2()
var press_threshold = 20

var swype = false
var swypePoint = null
var swypeDX = 0

func _gui_input(event):
	if event is InputEventMouseButton and not disabled: #with InputEventTouch it didn't want to work well
		if event.pressed:
			pressdown_position = event.position
		elif event.position.distance_to(pressdown_position) <= press_threshold:
			inputEvent(event)
	


func inputEvent(ev):
	
	swypePoint = ev.position.x
	
	get_node("../../MarginContainer/VBoxContainer/ScrollContainer").set_h_scroll(get_node("../../MarginContainer/VBoxContainer/ScrollContainer").get_h_scroll() - ev.position.x + swypePoint)
	swypeDX = ev.position.x - swypePoint
