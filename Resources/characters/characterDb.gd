extends Resource
class_name characterDb

export var char_id : = 0
export var char_name : = ""
export var name_class : = ""
export var type : = ""
export var url : = ""
export var body_char : = {
	"cape": "",
	"hair": "",
	"helmet": "",
	"head": "",
	"body": "",
	"armor": "",
	"weapon": "",
	"shield": "",
	"ear": "",
	"earrings": "",
	"mouth": "",
	"beard": "",
	"eyes": "",
	"eyebrows": ""
}
export var soucer_game_type: Resource
